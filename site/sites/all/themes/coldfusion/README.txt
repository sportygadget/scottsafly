--- WHAT IS coldfusion_starter_lite? ---

coldfusion_starter_lite is a subtheme of the Fusion core theme (http://www.drupal.org/project/coldfusion). If you want to create a subtheme of Fusion, you can start here! This theme is designed to have minimal markup for ease of personal customization. If you want a full markup theme, please see coldfusion_starter.

-- CREATING A SUBTHEME --

First and foremost, copy this directory (/sites/all/themes/coldfusion_starter_lite) to your themes directory and rename it to a new theme name. This prevents your subtheme from being overwritten when Fusion Core is updated.

Please see the README.txt of the Fusion Core directory for basic information on creating a subtheme, or go directly to http://coldfusiondrupalthemes.com/support/theme-developers/subtheming-quickstart for help jumpstarting your theme.
~

